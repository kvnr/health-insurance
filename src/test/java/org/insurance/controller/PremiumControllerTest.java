package org.insurance.controller;

import org.insurance.model.Person;
import org.insurance.repository.PersonRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest(value = PremiumController.class, secure = false)
public class PremiumControllerTest {
    @Autowired
private MockMvc mockMvc;
    @MockBean
    private PersonRepository personRepository;
    @Test
    public void getPremiumDetails(String name) throws Exception {
        Person person = new Person();
        Mockito.when(
                personRepository.findOne(Mockito.anyString())).thenReturn(person);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/person/insuranceQuote/Gomes").accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse());
        String expected = "6856";

        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);

    }
}
