package org.insurance.service;

import org.insurance.model.Person;

public interface PersonService {
    public boolean addPerson(Person person);

}
