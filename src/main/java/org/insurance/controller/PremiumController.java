package org.insurance.controller;

import org.insurance.model.Person;
import org.insurance.repository.PersonRepository;
import org.insurance.service.PersonService;
import org.insurance.util.PremiumCalculator;
import org.insurance.util.PremiumDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/premium")
public class PremiumController {
    int basicPremiumAmt = 5000;
    @Autowired
    PersonRepository personRepository;
    @PostMapping("/add")
    public ResponseEntity<Void> addPerson(@RequestBody Person person) {
        person = personRepository.save(person);
        if (person == null) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
       // headers.setLocation(builder.path("/article/{id}").buildAndExpand(person.getArticleId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @PutMapping("/update/{name}")
    public ResponseEntity<Person> updatePerson(@PathVariable(value = "name") String name,@RequestBody Person person) {
       Person p = personRepository.findOne(name);
        p.setAge(person.getAge());
        p.setCurrentHealth(person.getCurrentHealth());
        p.setHabits(person.getHabits());
        p = personRepository.save(p);
        if (p == null) {
            return new ResponseEntity<Person>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        return ResponseEntity.ok(p);
    }

    @GetMapping("/details")
    public List<Person> getPremiumDetails() {
        return personRepository.findAll();
    }

    @GetMapping("/insuranceQuote/{name}")
    public int getInsuranceQuote(@PathVariable(value = "name") String name) {
        Person p = personRepository.findOne(name);
        return PremiumCalculator.getInsuranceQuote(basicPremiumAmt,p.getGender(),p.getAge(),p.getCurrentHealth(),p.getHabits());
    }

    @DeleteMapping("/delete/{name}")
    public ResponseEntity<Person> deletePerson(@PathVariable(value = "name") String name) {
        Person p = personRepository.findOne(name);
        if(p == null) {
            return ResponseEntity.notFound().build();
        }

        personRepository.delete(p);
        return ResponseEntity.ok().build();
    }

}
