package org.insurance.util;

import org.insurance.model.CurrentHealth;
import org.insurance.model.Habits;

public class PremiumCalculator {

    public static int getInsuranceQuote(int basePreimumAmt,String gender, int age, CurrentHealth currentHealth, Habits habits){
        int insuranceAmt =0;
        if(age>=18 || age<25){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*10/100);
        }
        if(age>=25 || age<30){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*10/100);
        }
        if(age>=30 || age<35){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*10/100);
        }
        if(age>=35 || age<40){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*10/100);
        }
        if(age>=40){
            if(age>=40 || age<45){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }

            if(age>=45 || age<50){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=50 || age<55){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=55 || age<60){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=60 || age<65){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=65 || age<70){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=70 || age<75){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=75 || age<80){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=80 || age<85){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=85 || age<90){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=90 || age<95){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
            if(age>=95 || age<100){
                basePreimumAmt=basePreimumAmt+(basePreimumAmt*20/100);
            }
        }

        if(gender.equals('M')){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*2/100);
        }
        if(currentHealth.getBloodPleasure().equals("Yes")){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*1/100);
        }
        if(currentHealth.getBloodSugar().equals("Yes")){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*1/100);
        }
        if(currentHealth.getHyperTension().equals("Yes")){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*1/100);
        }
        if(currentHealth.getOverWeight().equals("Yes")){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*1/100);
        }

        if(habits.getDailyExcercise().equals("Yes")){
            basePreimumAmt=basePreimumAmt-(basePreimumAmt*3/100);
        }
        if(habits.getDailyExcercise().equals("Yes")){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*3/100);
        }
        if(habits.getAlcohol().equals("Yes")){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*3/100);
        }
        if(habits.getSmoking().equals("Yes")){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*3/100);
        }
        if(habits.getDrugs().equals("Yes")){
            basePreimumAmt=basePreimumAmt+(basePreimumAmt*3/100);
        }
        return basePreimumAmt;

    }
}
