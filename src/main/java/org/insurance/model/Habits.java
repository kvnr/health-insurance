package org.insurance.model;

import javax.persistence.*;

@Entity
@Table(name = "habits")
public class Habits {
    private String smoking;
    private String alcohol;
    private String dailyExcercise;
    private String drugs;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private int person_id;

    public int getPerson_id() {
        return person_id;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getAlcohol() {
        return alcohol;
    }

    public void setAlcohol(String alcohol) {
        this.alcohol = alcohol;
    }

    public String getDailyExcercise() {
        return dailyExcercise;
    }

    public void setDailyExcercise(String dailyExcercise) {
        this.dailyExcercise = dailyExcercise;
    }

    public String getDrugs() {
        return drugs;
    }

    public void setDrugs(String drugs) {
        this.drugs = drugs;
    }

    public Habits(){
        
    }

    public Habits(String smoking, String alcohol, String dailyExcercise, String drugs, int person_id) {
        this.smoking = smoking;
        this.alcohol = alcohol;
        this.dailyExcercise = dailyExcercise;
        this.drugs = drugs;
        this.person_id = person_id;
    }
}
