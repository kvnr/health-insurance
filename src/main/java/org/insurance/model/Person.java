package org.insurance.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="persons")
public class Person implements Serializable{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="person_id")
    private int personId;
    private String name;
    private String gender;
    private int age;
    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "person")
    private CurrentHealth currentHealth;
    @OneToOne(fetch = FetchType.LAZY,
            cascade =  CascadeType.ALL,
            mappedBy = "person")
    private Habits habits;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public CurrentHealth getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(CurrentHealth currentHealth) {
        this.currentHealth = currentHealth;
    }

    public Habits getHabits() {
        return habits;
    }

    public void setHabits(Habits habits) {
        this.habits = habits;
    }
    public Person() {

    }
    public Person(int personId, String name, String gender, int age, CurrentHealth currentHealth, Habits habits) {
        this.personId = personId;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.currentHealth = currentHealth;
        this.habits = habits;
    }
}
