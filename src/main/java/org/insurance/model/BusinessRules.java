package org.insurance.model;

public class BusinessRules {
    private String basePremiumBeowAge18;
    private String basePremiumPercenageOfIncrease18to25;
    private String basePremiumPercenageOfIncrease25to30;
    private String basePremiumPercenageOfIncrease30to35;
    private String basePremiumPercenageOfIncrease35to40;
    private String basePremiumPercenageOfIncrease40above;

    public String getBasePremiumBeowAge18() {
        return basePremiumBeowAge18;
    }

    public void setBasePremiumBeowAge18(String basePremiumBeowAge18) {
        this.basePremiumBeowAge18 = basePremiumBeowAge18;
    }

    public String getBasePremiumPercenageOfIncrease18to25() {
        return basePremiumPercenageOfIncrease18to25;
    }

    public void setBasePremiumPercenageOfIncrease18to25(String basePremiumPercenageOfIncrease18to25) {
        this.basePremiumPercenageOfIncrease18to25 = basePremiumPercenageOfIncrease18to25;
    }

    public String getBasePremiumPercenageOfIncrease25to30() {
        return basePremiumPercenageOfIncrease25to30;
    }

    public void setBasePremiumPercenageOfIncrease25to30(String basePremiumPercenageOfIncrease25to30) {
        this.basePremiumPercenageOfIncrease25to30 = basePremiumPercenageOfIncrease25to30;
    }

    public String getBasePremiumPercenageOfIncrease30to35() {
        return basePremiumPercenageOfIncrease30to35;
    }

    public void setBasePremiumPercenageOfIncrease30to35(String basePremiumPercenageOfIncrease30to35) {
        this.basePremiumPercenageOfIncrease30to35 = basePremiumPercenageOfIncrease30to35;
    }

    public String getBasePremiumPercenageOfIncrease35to40() {
        return basePremiumPercenageOfIncrease35to40;
    }

    public void setBasePremiumPercenageOfIncrease35to40(String basePremiumPercenageOfIncrease35to40) {
        this.basePremiumPercenageOfIncrease35to40 = basePremiumPercenageOfIncrease35to40;
    }

    public String getBasePremiumPercenageOfIncrease40above() {
        return basePremiumPercenageOfIncrease40above;
    }

    public void setBasePremiumPercenageOfIncrease40above(String basePremiumPercenageOfIncrease40above) {
        this.basePremiumPercenageOfIncrease40above = basePremiumPercenageOfIncrease40above;
    }
}
