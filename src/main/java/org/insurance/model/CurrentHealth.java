package org.insurance.model;

import javax.persistence.*;

@Entity
@Table(name="current_health")
public class CurrentHealth {
    private String hyperTension;
    private String bloodPleasure;
    private String bloodSugar;
    private String overWeight;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private int person_id;

    public int getPerson_id() {
        return person_id;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public String getHyperTension() {
        return hyperTension;
    }

    public void setHyperTension(String hyperTension) {
        this.hyperTension = hyperTension;
    }

    public String getBloodPleasure() {
        return bloodPleasure;
    }

    public void setBloodPleasure(String bloodPleasure) {
        this.bloodPleasure = bloodPleasure;
    }

    public String getBloodSugar() {
        return bloodSugar;
    }

    public void setBloodSugar(String bloodSugar) {
        this.bloodSugar = bloodSugar;
    }

    public String getOverWeight() {
        return overWeight;
    }

    public void setOverWeight(String overWeight) {
        this.overWeight = overWeight;
    }
    public CurrentHealth(){

    }
    public CurrentHealth(String hyperTension, String bloodPleasure, String bloodSugar, String overWeight, int person_id) {
        this.hyperTension = hyperTension;
        this.bloodPleasure = bloodPleasure;
        this.bloodSugar = bloodSugar;
        this.overWeight = overWeight;
        this.person_id = person_id;
    }
}
