THE PROBLEM

Norman Gomes is looking for a health insurance quote using this application.


INPUT


Name: Norman Gomes
Gender: Male
Age: 34 years
Current health:


Hypertension: No
Blood pressure: No
Blood sugar: No
Overweight: Yes


Habits:


Smoking: No
Alcohol: Yes
Daily exercise: Yes
Drugs: No






OUTPUT


Health Insurance Premium for Mr. Gomes: Rs. 6,856



BUSINESS RULES


Base premium for anyone below the age of 18 years = Rs. 5,000
% increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males
Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
Habits


Good habits (Daily exercise) -> Reduce 3% for every good habit
Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit